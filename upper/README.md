# upper
File uploader / Image capture / text upload script

For [pomf](https://github.com/pomf/pomf) / [hastebin](https://github.com/zneix/haste-server) / etc

## Config

`pomf_url` to your pomf server. script adds `/upload.php` to end

`paste_url` to haste server, api location to `paste_api`

`capture_cmd` basically runs a command with a filename as its argument, and this file will then be uploaded

`edit_cmd` runs whatever program with the same argument as `capture_cmd`, after capture and before uploading

## reqs
requirest at least
`zenity xclip curl libnotify-tools jq`
and `ksnip`, `spectacle` or similar capture tool