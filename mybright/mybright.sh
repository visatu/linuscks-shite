#!/bin/bash

# Monitor brightnes controle using ddcutil :D

# What to identify with, eg argument for display identification
# ( https://www.ddcutil.com/display_selection/ )
id_arg="mfg"

# Extra args for ddcutil
args="--sleep-multiplier 0.01 --less-sleep --async --noverify"

# Monitor identifiers
LEFT="ENC"
CENTER="ACI"
RIGHT="DEL"

# where  to look for / store temp file
TMP_FILE="/tmp/ddcprev"

if [[ -f $TMP_FILE ]]; then
    DDC_PREV=$(< $TMP_FILE)
else
    DDC_PREV=$CENTER
fi

echo "Monitor control: setting $1"
case "$1" in
0)
    echo "Side monitors to 0 brightness.."
    ddcutil setvcp $args --$id_arg $LEFT 10 0
    ddcutil setvcp $args --$id_arg $RIGHT 10 0
    # Assuming we want to adjust center monitor next.. :D
    DDC_PREV=$CENTER
    ;;
1)
    echo "Left monitor to low brigtness..."
    ddcutil setvcp $args --$id_arg $LEFT 10 10
    DDC_PREV=$LEFT
    ;;
2)
    echo "Center monitor to low brigtness..."
    ddcutil setvcp $args --$id_arg $CENTER 10 10
    DDC_PREV=$CENTER
    ;;
3)
    echo "Rigth monitor to low brigtness..."
    ddcutil setvcp $args --$id_arg $RIGHT 10 10
    DDC_PREV=$RIGHT
    ;;
4)
    echo "Left monitor to mid brigtness..."
    ddcutil setvcp $args --$id_arg $LEFT 10 20
    DDC_PREV=$LEFT
    ;;
5)
    echo "Center monitor to mid brigtness..."
    ddcutil setvcp $args --$id_arg $CENTER 10 20
    DDC_PREV=$CENTER
    ;;
6)
    echo "Rigth monitor to mid brigtness..."
    ddcutil setvcp $args --$id_arg $RIGHT 10 30
    DDC_PREV=$RIGHT
    ;;
7)
    echo "Left monitor to top brigtness..."
    ddcutil setvcp $args --$id_arg $LEFT 10 50
    DDC_PREV=$LEFT
    ;;
8)
    echo "Center monitor to top brigtness..."
    ddcutil setvcp $args --$id_arg $CENTER 10 50
    DDC_PREV=$CENTER
    ;;
9)
    echo "Right monitor to top brigtness..."
    ddcutil setvcp $args --$id_arg $RIGHT 10 70
    DDC_PREV=$RIGHT
    ;;
pplus)
    echo "Prev monitor +"
    ddcutil setvcp $args --$id_arg $DDC_PREV 10 + 5
    ;;
pminus)
    echo "Prev monitor -"
    ddcutil setvcp $args --$id_arg $DDC_PREV 10 - 5
    ;;
*)
    echo paska
    ;;
esac

echo $DDC_PREV > $TMP_FILE

# ddcutil setvcp $args --mfg "HPN" 10 0 && export DDC_PREV="HPN"
# ddcutil setvcp $args --mfg "HPN" 10 0 && export DDC_PREV="HPN"
