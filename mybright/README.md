
# mybright.sh

Monitor brightness controll using [ddcutil](https://www.ddcutil.com/).

Example KDE shortcuts in `mybright_kglobalshortcutsrc`

## Requirements
At least the following
```
ddcutil i2c-tools
```

## Configuration

### load `i2c-dev` kernel module

Check if i2c devices can be found under `/dev`
```sh
ls /dev/i2c*

# Make i2c_dev kernel module load on boot (if doesn't already...)
sudo printf "# load i2c_dev on boot\ni2c_dev" > /etc/modules-load.d/i2c_dev.conf
```

```sh


# Add i2c group
sudo groupadd --system i2c

#Add yourself to i2c group
sudo usermod your-user-name -aG i2c

# Add udev role to get permissions for i2c group
sudo cp /usr/share/ddcutil/data/45-ddcutil-i2c.rules /etc/udev/rules.d

# NVIDIA stuff needs this apparently
sudo cp /usr/share/ddcutil/data/90-nvidia-i2c.conf /etc/X11/xorg.conf.d

# NVIDIA suff needs this as well apparently...
sudo printf "options nvidia NVreg_RegistryDwords=RMUseSwI2c=0x01;RMI2cSpeed=100" > /etc/modprobe.d/60-nvidia-i2c-ddcutil.conf

# reboot :)

# check if worked :D
ddcutil detect --verbose

```

For more details see [ddcutil installation documentation](https://www.ddcutil.com/config/)